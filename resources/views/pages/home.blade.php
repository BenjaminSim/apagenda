@extends('app')
@section('stylesheet')
    <link rel="stylesheet" href="/assets/css/home.css">
@endsection
@section('content')
    <section id="jumb">
        <div class="jump_content">
            <div class="informations">
                <span class="title"><h1>Bienvenue sur APAgenda</h1></span>
            </div>
            <div class="search">
                <div class="search_location">
                    <div class="search_location_input">
                        <input type="text" placeholder="Ou ksé ksé ?">
                        <span style="">ICO</span>
                    </div>
                </div>
                <div class="search_button">
                    <button>Activité physique</button>
                    <button>Rééducation</button>
                </div>
            </div>
        </div>
    </section>
@endsection
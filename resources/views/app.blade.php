<!doctype html>
<html lang="fr">
<head>
    @yield('stylesheet')
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/common.css">
    <title>Apagenda - Bienvenue</title>
</head>
<body>
    <div class="container">
        @yield('content')
    </div>
</body>
</html>